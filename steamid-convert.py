# Steam ID <-> Community ID converter

import sys, os, re

SID_NONE    = 1<<0
SID_PRINT   = 1<<1
SID_PROCESS = 1<<2
SID_QUIET   = 1<<3

def about():
    print "Steam Id <-> Community ID Converter\n" \
          "Programmed by theY4Kman, algorithm discovered by voogru\n"

def syntax(command=None):
    if command == ["help", "?", "/?"]:
        print "Displays the help message."
    else:
        print "Usage:  ", os.path.basename(sys.argv[0]), " SteamID|CommunityID [/p] [/q]\n" \
              "Options:\n" \
              "         /p  Show the process of deriving the Steam/Community ID\n" \
              "         /q  Quiet mode. Only print out the Steam/Community ID"

def proc(message, check=SID_NONE, flags=SID_NONE):
    if flags & check: print message

def convert(i, flags=SID_NONE):
    steamid = re.compile(r'STEAM_0:([01]):(\d{1,10})').match(i)
    commid  = re.compile(r"(\d{17})").match(i)
    if steamid:
        proc("Steam ID -> Community ID\n\n%s" % i, SID_PROCESS, flags)
        tok = steamid.groups()
        auth = int(tok[1]) * 2
        server = int(tok[0])
        proc("%s * 2 = %d" % (tok[1], auth), SID_PROCESS, flags)
        tok = auth + 76561197960265728 + server
        proc("%d + 76561197960265728 + %d = %d" % (auth, server, tok), SID_PROCESS, flags)
        if flags & SID_PRINT: print "\nCommunity ID:", tok
        else: return tok
    else:
        proc("Community ID -> Steam ID\n", SID_PROCESS, flags)
        tok = int(i)
        server = tok % 2
        proc("%s is %s, so the auth server is %d" % (i, (server and "odd") or "even", server), SID_PROCESS, flags)
        auth = tok - server
        proc("%d - %d = %d" % (tok, server, auth), SID_PROCESS, flags)
        auth -= 76561197960265728
        proc("%d - 76561197960265728 = %d" % (tok - server, auth), SID_PROCESS, flags)
        proc("%d / 2 = %d" % (auth, auth/2), SID_PROCESS, flags)
        auth /= 2

        # original
        # if flags & SID_PRINT: print "\nSteam ID: STEAM_0:%d:%d" % (server, auth)

        # just print SteamID
         if flags & SID_PRINT: print "STEAM_0:%d:%d" % (server, auth)

        else: return "STEAM_0:%d:%d" % (server, auth)
    return
if __name__ == '__main__':
    if len(sys.argv) < 2:
        syntax()
        exit()
    if sys.argv[1] == "about":
        about()
        exit()
    if sys.argv[1] in ["help", "?", "/?"]:
        syntax((len(sys.argv) >= 3) and sys.argv[2] or None)
        exit()

    flags = SID_NONE
    i = None
    for option in sys.argv[1:]:
        if option == "/p": flags |= SID_PROCESS
        if option == "/q": flags |= SID_QUIET
        else:
            if not i and re.compile(r'STEAM_0:[01]:\d{1,10}').match(option) or re.compile(r"\d{17}").match(option): i = option

    if not i:
        syntax()
    else:
        if flags & (SID_QUIET|SID_PROCESS) == (SID_QUIET|SID_PROCESS): flags ^= SID_PROCESS
        convert(i, flags|SID_PRINT)