@echo off

rem url of group
rem e.g. http://steamcommunity.com/groups/MEMEWARE3/
set arg1=%1

rem number of pages e.g. "3"
rem check manually (http://steamcommunity.com/groups/MEMEWARE3/memberslistxml/?xml=1)
rem each page has 1,000 IDs
set arg2=%2

rem need wget.exe in path!
wget -O %CD%\download%arg2%.txt "%arg1%/memberslistxml/?xml=1&p=%arg2%"