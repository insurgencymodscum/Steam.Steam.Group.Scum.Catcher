# About #

Get all Steam Community Group members.

python2.7.exe and wget.exe need to be in your path!


# Example #

Check number of pages ("totalPages"):

`http://steamcommunity.com/groups/MEMEWARE3/memberslistxml/?xml=1`

(Each page lists 1,000 IDs.)

Downloading each page:

```
1.bat http://steamcommunity.com/groups/MEMEWARE3 1
1.bat http://steamcommunity.com/groups/MEMEWARE3 2
1.bat http://steamcommunity.com/groups/MEMEWARE3 3
```

Concatenating:

```
2.bat
```

Extracting Steam Community IDs (SteamID64):

```
3.bat
```

Converting Steam Community ID (SteamID64) to STEAM_:

```
4.bat
```

# Checking #

output4.txt should look like:

```
STEAM_0...
7656...
...
```